package exceptions;

public class SameSpotException extends Exception{
	public SameSpotException(Integer x, Integer y){
		super("Object already at: " + x + ", " + y);
		System.out.println(getMessage());
	}
}
