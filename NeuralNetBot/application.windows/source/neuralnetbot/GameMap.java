package neuralnetbot;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.NavigableMap;
import java.util.TreeMap;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Text;

import exceptions.*;


public class GameMap {
	ArrayList<ArrayList<WorldObject>> objectMap; // Object in location
	ArrayList<Plant> plants;
	ArrayList<WorldObject> objects;
	ArrayList<Garden> gardens;
	Robot player;
	MainWindow parent;
	boolean drawPlantMode = false;
	boolean drawObstacleMode = false;
	boolean drawGardenMode = false;
	boolean runMode = false;
	boolean pressed = false;
	Integer x1, x2, y1, y2;
	Integer xreal, yreal, width, height;
				 				
		
	GameMap(MainWindow p){
		parent = p;
		objectMap = new ArrayList<ArrayList<WorldObject>>();
		System.out.println("xSize: " + parent.width + "ySize: " + parent.height);
		for (int i = 0; i < parent.width; i++){
			objectMap.add(new ArrayList<WorldObject>());
			for (int j = 0; j < parent.height; j++){
				objectMap.get(i).add((WorldObject)null);
			}
		}
		objects = new ArrayList<WorldObject>();
		gardens = new ArrayList<Garden>();
		
		createPlayer(100, 650);
			// TODO Auto-generated catch block
		try {
			createGarden(900, 50, 50, 50);
		} catch (SameSpotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createPlayer(Integer x, Integer y){
		player = new Robot(parent,this, x, y);
		objectMap.get(x).set(y, player);
	}
	
	public void createObstacle(Integer x, Integer y, Integer width, Integer height) throws SameSpotException{
		if (objectMap.get(x).get(y) == null){

		}else{
			throw new SameSpotException(x,y);
		}
		Obstacle temp = new Obstacle (parent, x, y, width, height);
		for (int i = x; i <= width + x; i++){
				objectMap.get(i).set(y, temp);
				objectMap.get(i).set(y+height, temp);
		}
		for (int j = y; j <= height + y; j++){
			objectMap.get(x).set(j, temp);
			objectMap.get(x+width).set(j, temp);
		}
		print();
		objects.add(temp);
	}
	public void createGarden(Integer x, Integer y, Integer width, Integer height) throws SameSpotException{
		if (objectMap.get(x).get(y) == null){

		}else{
			throw new SameSpotException(x,y);
		}
		Garden temp = new Garden(parent, x, y, width, height);
		for (int i = x; i <= width + x; i++){
				objectMap.get(i).set(y, temp);
				objectMap.get(i).set(y+height, temp);
		}
		for (int j = y; j <= height + y; j++){
			objectMap.get(x).set(j, temp);
			objectMap.get(x+width).set(j, temp);
		}
		print();
		objects.add(temp);
		gardens.add(temp);
	}

	public void createPlant(Integer x, Integer y) throws SameSpotException{
		if (objectMap.get(x).get(y) == null){

		}else{
			throw new SameSpotException(x,y);
		}
		Plant temp = new Plant (parent, x, y);
		objectMap.get(x).set(y, temp);
		objects.add(temp);
	}
	
	public void draw(){
		for (ArrayList<WorldObject> i : objectMap){
			for (WorldObject j: i){
				if (j != null){
					j.draw();
				}
			}
		}
		drawStatusBar();
		keyListener();
		if (drawPlantMode){
			drawPlant();
		}
		if (drawObstacleMode){
			drawObstacle();
		}
		if (drawGardenMode){
			drawGarden();
		}
		if (runMode){
			player.run();
		}
		//sense();
		//player.run();
	}
	
	
	public void drawStatusBar(){
		parent.fill(210);
		parent.rectMode(parent.CORNER);
		parent.rect(0, parent.getHeight()-30, parent.getWidth(), 30);
		for (WorldObject o: objects){
			if (o.isSelected()){
				parent.fill(0);
				parent.text(o.getStats(), 10, parent.getHeight()-10);
			}
			
		}
	}
	
	private void drawPlant(){
		if (parent.mousePressed && parent.mouseButton == parent.LEFT){
			try {
				createPlant(parent.mouseX, parent.mouseY);
			} catch (SameSpotException e) {

			}
		}
	}

	private void drawObstacle(){

		if (parent.mousePressed && parent.mouseButton == parent.LEFT && !pressed){
			x1 = x2 = parent.mouseX;
			y1 = y2 = parent.mouseY;
			pressed = true;
		}
		if (parent.mousePressed && pressed){
			x2 = parent.mouseX;
			y2 = parent.mouseY;
			parent.text("In While loop", 10, parent.getHeight()-10);
		}
		if (!parent.mousePressed && pressed){
			pressed = false;
			

			try {
				if (x1 < x2){
					xreal = x1;
					width = x2-x1;
				}else{
					xreal = x2;
					width = x1 - x2;

				}
				if (y1 < y2){
					yreal = y1;
					height = y2-y1;
				}else{
					yreal = y2;
					height = y1-y2;
				}
				createObstacle(xreal, yreal, width, height);
			} catch (SameSpotException e) {

			}
		}
	}
	
	private void drawGarden(){

		if (parent.mousePressed && parent.mouseButton == parent.LEFT && !pressed){
			x1 = x2 = parent.mouseX;
			y1 = y2 = parent.mouseY;
			pressed = true;
		}
		if (parent.mousePressed && pressed){
			x2 = parent.mouseX;
			y2 = parent.mouseY;
			parent.text("In While loop", 10, parent.getHeight()-10);
		}
		if (!parent.mousePressed && pressed){
			pressed = false;
			

			try {
				if (x1 < x2){
					xreal = x1;
					width = x2-x1;
				}else{
					xreal = x2;
					width = x1 - x2;

				}
				if (y1 < y2){
					yreal = y1;
					height = y2-y1;
				}else{
					yreal = y2;
					height = y1-y2;
				}
				createGarden(xreal, yreal, width, height);
			} catch (SameSpotException e) {

			}
		}
	}

	public void setDrawPlant(boolean b){
		drawPlantMode = b;
	}

	public void keyListener(){
		if (parent.keyPressed){
			if (parent.key == parent.CODED){
				if (parent.keyCode == parent.UP){
					player.move(0, -1);
				}
				if (parent.keyCode == parent.DOWN){
					player.move(0, 1);
				}
				if (parent.keyCode == parent.LEFT){
					player.move(-1, 0);
				}
				if (parent.keyCode == parent.RIGHT){
					player.move(1, 0);
				}
			}
		}
	}
	
	public void setDrawObstacle(boolean b){
		drawObstacleMode = b;
	}
	
	public void setDrawGarden(boolean b){
		drawGardenMode = b;
	}
	
	public void sense(){
		for (CollisionSensor s: player.sensors){
			Double output;
			output = s.getOutput();
			if (output > 0){
				System.out.println("Output of sensor = " + output);
			}
		}
	}
	
	public void print(){
		System.out.print("\n");
		for (int i = 0; i < objectMap.size(); i++ ){
			for (int j = 0; j < objectMap.get(i).size(); j++){
				if (objectMap.get(i).get(j) != null){
					System.out.print("*");
				}
			}
			System.out.print("\n");
		}
	}

	public void setRunMode(boolean b) {
		// TODO Auto-generated method stub
		runMode = b;
	}
	
	
	
	
}
