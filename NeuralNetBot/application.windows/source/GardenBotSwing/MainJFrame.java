package GardenBotSwing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import neuralnetbot.GameMap;
import neuralnetbot.MainWindow;

import processing.core.PApplet;

public class MainJFrame extends JFrame {
	javax.swing.JPanel mainWindowPanel;
	JMenuBar menuBar;
	public GameMap gameMap;



	public MainJFrame (MainWindow w){
		JPanel contentPane = new JPanel();
		this.setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout());
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setSize(1300, 800); //The window Dimensions
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		mainWindowPanel = new javax.swing.JPanel();
		mainWindowPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		MainWindow sketch = w;
		
		mainWindowPanel.setSize(1300, 800);
		mainWindowPanel.add(sketch);
		contentPane.add(mainWindowPanel, BorderLayout.WEST);
		gameMap = sketch.gameMap;
		if (gameMap == null){System.out.println("Is Null");}
		menuBar = new MainMenuBar(gameMap);
		setJMenuBar(menuBar);
	}


}