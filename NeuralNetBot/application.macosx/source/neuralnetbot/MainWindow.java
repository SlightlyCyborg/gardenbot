package neuralnetbot;

import java.util.ArrayList;

import GardenBotSwing.MainJFrame;
import processing.core.PApplet;
import processing.core.PImage;


public class MainWindow extends PApplet {
	public GameMap gameMap;
	PImage bg;
	
	
	public void setup() {
		System.out.println("ran setup");
		background(255);
		size(1200,750);
		gameMap = new GameMap(this);
		bg = loadImage("myHouse.jpg");
		MainJFrame frame = new MainJFrame(this);
		frame.setVisible(true);
	}

	public void draw() {
		background(bg);
		gameMap.draw();
	}
	
	public GameMap getGameMap(){
		if (gameMap == null){
			System.out.println("Is null in function");
		}
		return gameMap;
	}
	  public static void main(String args[])
	    {
	      PApplet.main(new String[] { neuralnetbot.MainWindow.class.getName() });
	    }
	
}