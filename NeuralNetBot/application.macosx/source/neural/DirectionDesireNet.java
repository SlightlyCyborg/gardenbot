package neural;

import java.util.Map.Entry;
import java.util.TreeMap;
import java.lang.Math;

public class DirectionDesireNet extends NeuralNet {
	Double degreesPerNeuron;
	TreeMap<Double, Neuron> anglesAndNeurons;
	
	DirectionDesireNet(Integer n){
		anglesAndNeurons = new TreeMap<Double, Neuron>();
		degreesPerNeuron =  360.0 / (double)n;
		System.out.println("Degrees/neuron =" + degreesPerNeuron);
		Neuron temp;
		for (int i = 0; i < n; i++){
			temp = new Neuron();
			add(temp);
			anglesAndNeurons.put(i * degreesPerNeuron, temp);
		}
	}
	
	public void linkNeurons(){
		Double weight;
		for (Entry<Double, Neuron> i: anglesAndNeurons.entrySet()){
			for (Entry<Double, Neuron> j: anglesAndNeurons.entrySet()){
				weight = 0.0;
				weight = (j.getValue().output)*(Math.cos(Math.toRadians(Math.abs(i.getKey()-j.getKey()))));
				if (Math.abs(weight) < 0.00001){
					weight = 0.0;
				}
				System.out.print(weight + ", ");
				if (i.getValue() != j.getValue()){
				 i.getValue().addInput(j.getValue(), weight);
				}
				
				
			}
			System.out.println();
		}
	}
	
	public void printNet(){
		System.out.println("|  " + neurons.get(2) + "  |");
		System.out.println("| " + neurons.get(3) + " " + neurons.get(1) + " |");
		System.out.println("|" + neurons.get(4) + "   " + neurons.get(0) + "|");
		System.out.println("| " + neurons.get(5)+" " + neurons.get(7) + " |");
		System.out.println("|  " + neurons.get(6) + "  |");
	}
	
	public static void main(String args[]){
		DirectionDesireNet ddn = new DirectionDesireNet(8);
		ddn.linkNeurons();
		ddn.printNet();
		ddn.update();
		ddn.printNet();
		Neuron foodDesire = new Neuron();
		foodDesire.output = .1;
		ddn.neurons.get(0).addInput(foodDesire, .7);
		ddn.printNet();
		Neuron foodDesire2 = new Neuron();
		foodDesire2.output = .1;
		ddn.neurons.get(4).addInput(foodDesire2, .7);
		ddn.neurons.get(0).weights.get(3);
		ddn.update();
		ddn.printNet();
		
	}
	
}