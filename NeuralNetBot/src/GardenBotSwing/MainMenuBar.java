package GardenBotSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

import neuralnetbot.GameMap;

public class MainMenuBar extends JMenuBar{
	JMenu menu, submenu;
	JMenuItem menuItem;
	JRadioButtonMenuItem rbMenuItem;
	JCheckBoxMenuItem cbMenuItem, obMenuItem, gMenuItem, sMenuItem;
	GameMap gameMap;
	
	MainMenuBar(GameMap gm){
		gameMap = gm;
		
		//Build the first menu.
		menu = new JMenu("Main Menu");
		menu.setMnemonic(KeyEvent.VK_A);
		menu.getAccessibleContext().setAccessibleDescription(
		        "The only menu in this program that has menu items");
		this.add(menu);

		

	
		cbMenuItem = new JCheckBoxMenuItem("Draw Plant mode");
		cbMenuItem.setMnemonic(KeyEvent.VK_H);
		cbMenuItem.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				if (cbMenuItem.isSelected()){
					gameMap.setDrawPlant(true);
				}
				
				if (!cbMenuItem.isSelected()){
					gameMap.setDrawPlant(false);
				}
				
				
			}
			
		});
		menu.add(cbMenuItem);
		
		
		obMenuItem = new JCheckBoxMenuItem("Draw Obstacle mode");
		obMenuItem.setMnemonic(KeyEvent.VK_H);
		obMenuItem.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				if (obMenuItem.isSelected()){
					gameMap.setDrawObstacle(true);
				}
				
				if (!obMenuItem.isSelected()){
					gameMap.setDrawObstacle(false);
				}
				
				
			}
			
		});
		menu.add(obMenuItem);
		
		gMenuItem = new JCheckBoxMenuItem("Draw GardenMode");
		gMenuItem.setMnemonic(KeyEvent.VK_H);
		gMenuItem.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				if (gMenuItem.isSelected()){
					gameMap.setDrawGarden(true);
				}
				
				if (!gMenuItem.isSelected()){
					gameMap.setDrawGarden(false);
				}
				
				
			}
			
		});
		menu.add(gMenuItem);

		sMenuItem = new JCheckBoxMenuItem("Run NeuralControl");
		sMenuItem.setMnemonic(KeyEvent.VK_H);
		sMenuItem.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				if (sMenuItem.isSelected()){
					gameMap.setRunMode(true);
				}
				
				if (!sMenuItem.isSelected()){
					gameMap.setRunMode(false);
				}
				
				
			}
			
		});
		menu.add(sMenuItem);

	
	}
}
