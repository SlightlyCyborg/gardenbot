package neuralnetbot;

public class CollisionSensor {
	Integer x1, x2, y1, y2;
	Robot robot;
	GameMap map;
	Directions direction;
	Integer length;
	
	CollisionSensor(Robot r, Directions d,  int length){
		robot = r;
		this.length = length;
		map = robot.map;
		direction = d;
		if (d == Directions.LEFT){
			y1 = y2 = robot.Y;
			x1 = robot.X-robot.realRad;
			x2 = robot.X-robot.realRad-length;
			
		}
		if (d == Directions.RIGHT){
			y1 = y2 = robot.Y;
			x1 = robot.X+robot.realRad;
			x2 = robot.X+robot.realRad+length;
		}
		if (d == Directions.UP){
			x1 = x2 = robot.X;
			y1 = robot.Y-robot.realRad;
			y2 = robot.Y-robot.realRad-length;
		}
		if (d == Directions.DOWN){
			x1 = x2 = robot.X;
			y1 = robot.Y+robot.realRad;
			y2 = robot.Y+robot.realRad+length;
		}
	}
	
	public void draw(){
		
		robot.parent.strokeWeight(3);
		robot.parent.stroke(0);
		robot.parent.line(x1, y1, x2, y2);
		robot.parent.strokeWeight(1);
	}
	
	public void move(int x, int y){
		x1 += x;
		x2 += x;
		y1 += y;
		y2 += y;
	}
	
	public Double getOutput(){
		switch(direction){
		case LEFT:
			for (int i = x1; i >= x2; i--){
				if (map.objectMap.get(i).get(y1) != null){
					return 1.0 - (((double)Math.abs(i-x1))/((double)length));
				}
			}
			break;
		case RIGHT:
			for (int i = x1; i <= x2; i++){
				if (map.objectMap.get(i).get(y1) != null){
					return 1.0 - (((double)Math.abs(i-x1))/((double)length));
				}
			}
			break;
		case UP:
			for (int i = y1; i >= y2; i--){
				if (map.objectMap.get(x1).get(i) != null){
					return 1.0 - (((double)Math.abs(i-y1))/((double)length));
				}
			}
			break;
		case DOWN:
			for (int i = y1; i <= y2; i++){
				if (map.objectMap.get(x1).get(i) != null){
					return 1.0 - (((double)Math.abs(i-y1))/((double)length));
				}
			}
			break;
		}
		
		return 0.0;
	}
}
