package neuralnetbot;
import processing.core.PApplet;

public class WorldObject{
	PApplet parent;
	Integer X;
	Integer Y;
	Integer rad;
	Integer realRad;

	
	public WorldObject(PApplet parent, int x, int y){
		this.parent = parent;
		rad = 20;
		realRad = rad/2;
		X = x;
		Y = y;
	}
	
	public void move(int deltaX, int deltaY){
		X += deltaX;
		Y += deltaY;
	}
	
	public void draw(){
		parent.stroke(0);
		parent.fill(9, 0, 179);
		parent.ellipseMode(parent.CENTER);
		parent.ellipse(X, Y, rad, rad);
	}
	
	public boolean isSelected(){
		if((parent.mouseX <= X+(rad + 20) && parent.mouseX >= X-(rad+20)) && (parent.mouseY <= Y+(rad+20) && parent.mouseY >= Y-(rad+20))){
			return true;
		}else{
			return false;
		}
	}
	
	public String getStats(){
		String rv;
		rv = "Stats: X =" + X.toString() + ", Y:" + Y.toString();
		return rv;
	}
	
	

}
