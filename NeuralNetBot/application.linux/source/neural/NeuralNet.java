package neural;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class NeuralNet {
	TreeMap<Integer, Neuron> updateList;
	ArrayList<Neuron> neurons = new ArrayList<Neuron>();
	
	NeuralNet(){
		for (int i = 0; i < 10; i++){
			neurons.add(new Neuron());
		}
	}
	public void update(){
		Integer randomNum;
		Random rand = new Random();
		updateList = new TreeMap <Integer, Neuron>();
		for (Neuron n: neurons){
			randomNum = rand.nextInt();
			if (!updateList.containsKey(randomNum)){
				updateList.put(randomNum, n);
			}else{
				while (updateList.containsKey(randomNum)){
					randomNum = rand.nextInt();
				}
			}
		}
		
		
		for (Map.Entry<Integer, Neuron> entry: updateList.entrySet()){
			Neuron current;
			current = entry.getValue();
			current.update();
		}
	}
	
	public void add(Neuron n){
		neurons.add(n);
	}
	
	public static void main(String args[]){
		NeuralNet net = new NeuralNet();
		net.update();
	}
}

