package neural;

import java.util.ArrayList;
import java.util.Random;

public class Neuron {
	ArrayList<Neuron> inputs;
	ArrayList<Double> weights;
	Double output;
	
	Neuron(){
		inputs = new ArrayList<Neuron>();
		weights = new ArrayList<Double>();
		Random rand = new Random();	
		if (rand.nextBoolean()){ 
			output = 1.0;
		}else{
			output = -1.0;
		}
	}

	public void update(){
		Double sum = 0.0;
		for (int i = 0; i < inputs.size(); i++){
			sum += inputs.get(i).getOutput() * weights.get(i);
		}
		if (sum > 0){
			output = 1.0;
		}else{
			output = -1.0;
		}
	}
	
	public String toString(){
		String rv;
		if (output == 1){
			rv = "*";
		}else{
			rv = "-";
		}
		return rv;
	}
	
	public void addInput(Neuron n, Double w){
		inputs.add(n);
		weights.add(w);
	}

	private Double getOutput() {
		// TODO Auto-generated method stub
		return output;
	}
}
